QT                                   += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

LIBS           += -lGL -lGLU
CONFIG         += thread
QMAKE_CXXFLAGS += -std=c++14 -Wall
# This line is needed because QtCreator on my PC stopped looking up for STL in it's own directory
INCLUDEPATH    += /usr/include/c++/6.2.1/ ../include /usr/include/ ./ ./include/

TARGET = kilo
TEMPLATE = app
VERSION = 0.4.3

TRANSLATIONS += \
	qrc/lang/Kilo_en.ts \
	qrc/lang/Kilo_ru.ts

SOURCES += \
	src/Core.cpp \
	src/main.cpp \
	src/Widget3D.cpp \
	src/WidgetAxis.cpp \
	src/WidgetModel.cpp \
	src/WidgetParticle.cpp \
    src/MainWindow.cpp \
    src/WidgetMultiBox.cpp \
    src/Simulation.cpp \
    src/WidgetMultiBoxBar.cpp


HEADERS += \
	include/Core.hpp \
	include/Widget3D.hpp \
	include/WidgetAxis.hpp \
	include/WidgetModel.hpp \
	include/WidgetParticle.hpp \
    include/MainWindow.hpp \
    include/WidgetMultiBox.hpp \
    include/Simulation.hpp \
    include/WidgetMultiBoxBar.hpp

FORMS +=

RESOURCES += \
	qrc/resources.qrc

DISTFILES += \
	examples/a.kmf \
	examples/b.kmf \
	examples/c.kmf

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libkilo/release/ -lkilo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libkilo/debug/ -lkilo
else:unix: LIBS += -L$$OUT_PWD/../libkilo/ -lkilo
