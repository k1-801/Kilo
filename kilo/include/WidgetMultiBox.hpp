﻿#ifndef WIDGETMULTIBOX_HPP
#define WIDGETMULTIBOX_HPP

// Qt
#include <QObject>
#include <QWidget>
#include <QFrame>
#include <QComboBox>
#include <QPushButton>
// libkilo
#include <libkilo/Universe.hpp>

namespace Kilo
{
	class WidgetMultiBox;
}

#include "WidgetMultiBoxBar.hpp"

namespace Kilo
{
	class WidgetMultiBox : public QFrame
	{
		Q_OBJECT

		private:
			// Components for drawing
			QWidget*           _internal;
			WidgetMultiBoxBar* _bar;

			// ParticleWP   _drawingParticle;
			static const int barElSize = 16;
			// This method builds all the UI
			void create();

		public:
			explicit WidgetMultiBox(QWidget *parent = 0);
			explicit WidgetMultiBox(WidgetMultiBox&);

		public slots:
			void changeUniverse();
			void divideV();
			void divideH();
			void collapse();
	};
}

#endif // WIDGETMULTIBOX_HPP
