﻿#pragma once

#ifndef WIDGET_MODEL_HPP_INCLUDED
#define WIDGET_MODEL_HPP_INCLUDED

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

namespace Kilo
{
	class WidgetModel;
}

// libkilo
#include <libkilo/AbstractParticle.hpp>
#include <libkilo/Universe.hpp>
// Kilo
#include "Widget3D.hpp"

namespace Kilo
{
	class WidgetModel : public Widget3D
	{
		protected:
			Rotation _rotation; // Rotation angle
			Coord    _offset;
			long double   _zoom;

			// This function does all the drawing work
			void paintGL();

			// Each WidgetModel is supposed to draw one Particle attached to it, if there is one.
			// It should be the current Universe, but doesn't have to.
			// If it isn't, it's Universe should be stored in the _universe variable.
			// There also should be two buttons for moving up to the Particle's parent or Particle's Universe.
			AbstractParticle* _rootParticle;
			Universe        * _universe;

		public:
			WidgetModel(QWidget* = 0);
			~WidgetModel();
			// All these functions are DEPRECATED, use OpenGL3 Buffers, sphere mesh and resizable traectory buffer instead
			void drawSphere   (QColor, Coord, long double);
			void drawPoint    (QColor, Coord);
			// Temporarily no Traectory definition, check if this is needed
			//void drawTraectory(QColor, Traectory&);

		public slots:
			void changeAngle(Rotation);
			void changeZoom(long double);
	};
}

#endif // WIDGET_MODEL_HPP_INCLUDED
