﻿#ifndef KILO_SIMULATION_HPP
#define KILO_SIMULATION_HPP

// Qt
#include <QString>
// libkilo
#include <libkilo/Universe.hpp>

namespace Kilo
{
	class Simulation
	{
		private:
			QString  _server;
			QString  _name;
			Universe _universe;

		public:
			Simulation();
			QString& getName();
	};
}

#endif // KILO_SIMULATION_HPP
