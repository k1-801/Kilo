﻿#pragma once

#ifndef KILO_CORE_HPP_INCLUDED
#define KILO_CORE_HPP_INCLUDED

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 * Client-side Core class
 *
 * @project Kilo
 * @author k1-801
 */

// C
#include <stdint.h>
// C++
#include <list>
// Qt
#include <QObject>
#include <QMutex>
#include <QThread>
#include <QTranslator>
// Kilo
#include "MainWindow.hpp"

namespace Kilo
{
	class Core;
}

// libkilo
#include <libkilo/Universe.hpp>
#include <libkilo/Vector3.hpp>

namespace Kilo
{
	class Core : public QObject
	{
		Q_OBJECT

		protected:
			Core();
			~Core();
			MainWindow* _w;

		public:
			Coord cam;
			Rotation rotation;

			QMutex mutexParams;
			std::mutex mutexParticles;

			static Core& getInstance();
			QString getPath();

			void error(QString);
			void run();
	};
}

#define CATCH(fun) \
	catch(Exception& e) \
	{ \
		Core::getInstance().error(tr("%1(): %2").arg(fun).arg(e.getText())); \
	} \
	catch(std::exception& e) \
	{ \
		Core::getInstance().error(tr("%1() cought an std::exception: %2").arg(fun).arg(e.what())); \
	} \
	catch(...) \
	{ \
		Core::getInstance().error(tr("%1() cought an unknown exception").arg(fun)); \
	}

#endif // KILO_CORE_HPP_INCLUDED
