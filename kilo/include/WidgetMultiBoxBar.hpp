﻿#ifndef KILO_WIDGETMULTIBOXBAR_HPP
#define KILO_WIDGETMULTIBOXBAR_HPP

// Qt
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>

namespace Kilo
{
	class WidgetMultiBoxBar;
}

#include "WidgetMultiBox.hpp"

namespace Kilo
{
	class WidgetMultiBoxBar : public QWidget
	{
		Q_OBJECT

		private:
			void create();
			int _elemSize;

		public:
			QHBoxLayout* _hlay;
			QPushButton* _hdiv;
			QPushButton* _vdiv;
			QPushButton* _close;
			QComboBox*   _mode;
			QWidget*     _subbar;

			WidgetMultiBoxBar(WidgetMultiBox* parent);
			void setSubbar(QWidget*);
			void setElemSize(int);
	};
}

#endif // KILO_WIDGETMULTIBOXBAR_HPP
