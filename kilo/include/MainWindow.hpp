﻿#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QSplitter>

namespace Kilo
{
	class MainWindow : public QMainWindow
	{
		public:
			MainWindow();

		private slots:
			void about();
			//void save();
			//void open();

		private:
			QSplitter* splitter;
			void createActions();
			void createDock();
	};
	extern MainWindow* wind;
}

#endif // MAINWINDOW_HPP
