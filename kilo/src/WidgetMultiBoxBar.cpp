﻿#include "include/WidgetMultiBoxBar.hpp"

#include <QLabel>

namespace Kilo {

	WidgetMultiBoxBar::WidgetMultiBoxBar(WidgetMultiBox* parent) : QWidget(parent)
	{
		_elemSize = 16;
		create();
	}

	void WidgetMultiBoxBar::create()
	{
		_mode   = new QComboBox();
		_hdiv   = new QPushButton("|");
		_vdiv   = new QPushButton("—");
		_close  = new QPushButton("X");
		_subbar = new QLabel("No parameters");

		setBackgroundRole(QPalette::Button);
		setFixedHeight(_elemSize + 4);

		_hlay = new QHBoxLayout(this);
		_hlay->setMargin(2);
		_hlay->setSpacing(2);
		_hlay->addWidget(_mode);
		_hlay->addWidget(_subbar);
		_hlay->addWidget(_hdiv);
		_hlay->addWidget(_vdiv);
		_hlay->addWidget(_close);

		_mode ->setFixedHeight(_elemSize);
		_hdiv ->setFixedHeight(_elemSize);
		_vdiv ->setFixedHeight(_elemSize);
		_close->setFixedHeight(_elemSize);

		_mode ->setFixedWidth(_elemSize);
		_hdiv ->setFixedWidth(_elemSize);
		_vdiv ->setFixedWidth(_elemSize);
		_close->setFixedWidth(_elemSize);
	}

	void WidgetMultiBoxBar::setElemSize(int n)
	{
		_elemSize = n;
	}
} // namespace Kilo
