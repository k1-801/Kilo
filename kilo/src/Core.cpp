﻿#include "Core.hpp"

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * Kilo::Core - a class used to hold all important data - all simulated projects, windows and subwindows, Universes
 *
 * @project Kilo
 * @author k1-801
 */

// C++
#include <math.h>
// Qt
#include <QCoreApplication>
#include <QFile>
#include <QMessageBox>
#include <QMainWindow>
#include <QDockWidget>
// libkilo
#include <libkilo/Exception.hpp>
#include <libkilo/Phy.hpp>

namespace Kilo
{
	Core::Core()
	{
		_w = new MainWindow();
	}

	Core::~Core()
	{
		mutexParticles.unlock();
	}

	Core& Core::getInstance()
	{
		static Core _instance;
		return _instance;
	}

	void Core::run()
	{
		_w->show();
	}

	void Core::error(QString text)
	{
		if(text == QString::null){text = tr("Unknown error");}
		QMessageBox::critical(_w, "Kilo", text, QMessageBox::Ok);
	}
}
