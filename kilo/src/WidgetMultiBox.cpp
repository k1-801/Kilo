﻿#include "include/WidgetMultiBox.hpp"

// Qt
#include <QBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QSplitter>
#include <QPalette>
// Kilo
#include "MainWindow.hpp"
#include "WidgetModel.hpp"
#include <libkilo/Exception.hpp>

namespace Kilo
{
	WidgetMultiBox::WidgetMultiBox(QWidget *parent) : QFrame(parent)
	{
		create();
	}

	WidgetMultiBox::WidgetMultiBox(WidgetMultiBox&) : QFrame()
	{
		create();
		//_universe = old._universe;
	}

	void WidgetMultiBox::create()
	{
		setFocusPolicy(Qt::StrongFocus);
		setFrameShape(QFrame::Box);
		setFrameShadow(QFrame::Plain);
		setContentsMargins(0, 0, 0, 0);
		_bar      = new WidgetMultiBoxBar(this);
		_internal = new WidgetModel();

		connect(_bar->_hdiv,  &QPushButton::clicked, this, &WidgetMultiBox::divideH);
		connect(_bar->_vdiv,  &QPushButton::clicked, this, &WidgetMultiBox::divideV);
		connect(_bar->_close, &QPushButton::clicked, this, &WidgetMultiBox::collapse);

		QVBoxLayout* vlay = new QVBoxLayout(this);
		vlay->addWidget(_bar);
		vlay->addWidget(_internal);

		QSplitter* splitter = dynamic_cast<QSplitter*>(parentWidget());
		if(splitter)
		{
			splitter->setOpaqueResize(true);
			splitter->setHandleWidth(1);
		}
	}

	void WidgetMultiBox::divideH()
	{
		QSplitter* splitter = dynamic_cast<QSplitter*>(parentWidget());
		if(splitter)
		{
			if(splitter->orientation() == Qt::Vertical)
			{
				QSplitter* nsplitter = new QSplitter(Qt::Horizontal, splitter);
				splitter->insertWidget(splitter->indexOf(this), nsplitter);
				splitter = nsplitter;
				setParent(splitter);
			}
		}
		else
		{
			splitter = new QSplitter(Qt::Horizontal, (QWidget*)(parent()));
			setParent(splitter);
		}
		// Insert a widget into a splitter
		splitter->insertWidget(splitter->indexOf(this) + 1, new WidgetMultiBox(*this));
	}

	void WidgetMultiBox::divideV()
	{
		QSplitter* splitter = dynamic_cast<QSplitter*>(parentWidget());
		if(splitter)
		{
			if(splitter->orientation() == Qt::Horizontal)
			{
				QSplitter* nsplitter = new QSplitter(Qt::Vertical, splitter);
				splitter->insertWidget(splitter->indexOf(this), nsplitter);
				splitter = nsplitter;
				setParent(splitter);
			}
		}
		else
		{
			splitter = new QSplitter(Qt::Vertical, (QWidget*)(parent()));
			setParent(splitter);
		}
		// Insert a widget into a splitter
		splitter->insertWidget(splitter->indexOf(this) + 1, new WidgetMultiBox(*this));
	}

	void WidgetMultiBox::collapse()
	{
		QSplitter* splitter = dynamic_cast<QSplitter*>(this->parentWidget());
		this->deleteLater();
		if(splitter->count() == 1)
			splitter->deleteLater();
	}

	void WidgetMultiBox::changeUniverse()
	{
		;
	}
}
