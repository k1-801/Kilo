﻿#include "MainWindow.hpp"

#include <QFile>
// Kilo
#include <WidgetMultiBox.hpp>

namespace Kilo
{
	MainWindow* wind;

	MainWindow::MainWindow()
	{
		splitter = new QSplitter(this);
		setCentralWidget(splitter);
		createActions();
		createDock();
		wind = this;
		setWindowIcon(QIcon(QString(":/img/logo.png")));
		QFile qssfile(":/qss/MultiBox.qss");
		qssfile.open(QFile::ReadOnly);
		setStyleSheet(QString(qssfile.readAll()));
		qssfile.close();
	}

	void MainWindow::about()
	{
		// Spawn another window
	}

	void MainWindow::createActions()
	{
		// Spawn actions
	}

	void MainWindow::createDock()
	{
		new WidgetMultiBox(splitter);
	}
}
