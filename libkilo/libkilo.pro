#-------------------------------------------------
#
# Project created by QtCreator 2016-09-16T13:35:00
#
#-------------------------------------------------

QT       -= gui

TARGET = kilo
TEMPLATE = lib
INCLUDEPATH += /usr/include/c++/6.2.1/ /usr/include/ ./include ../include
VERSION = 0.1.0

DEFINES += KILO_LIBRARY

unix
{
    target.path = /usr/lib
    INSTALLS += target
}

HEADERS += \
    include/AbstractParticle.hpp \
    include/Exception.hpp \
    include/Factory.hpp \
    include/Factory.tpp \
    include/Hash.hpp \
    include/ParticleField.hpp \
    include/Universe.hpp \
    include/Vector3.hpp \
    include/Color.hpp \
    include/Operators.hpp \
    include/Defs.hpp \
    include/Phy.hpp \
    include/Dot.hpp \
    include/Locker2.hpp \
    include/ParticleGroup.hpp

SOURCES += \
    src/AbstractParticle.cpp \
    src/Exception.cpp \
    src/Hash.cpp \
    src/ParticleField.cpp \
    src/Universe.cpp \
    src/Vector3.cpp \
    src/Operators.cpp \
    src/Dot.cpp \
    src/Locker2.cpp \
    src/ParticleGroup.cpp \
    src/Color.cpp

