﻿#pragma once

#ifndef KILO_UNIVERSE_HPP
#define KILO_UNIVERSE_HPP

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * Universe is a particle that does NO physical interaction and is used as the global parent for all of the particles.
 *
 * @project Kilo
 * @author k1-801
 */

// Qt
#include <QObject>
#include <QString>
// libkilo
#include <libkilo/ParticleGroup.hpp>

namespace Kilo
{
	class Universe;
}

namespace Kilo
{
	class Universe : public ParticleGroup
	{
		Q_OBJECT

		public:
			Universe();
			~Universe();

			void updateGroup() override;
	};
}

#endif // KILO_UNIVERSE_HPP
