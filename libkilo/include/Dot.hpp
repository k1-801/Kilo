﻿#pragma once

#ifndef DOT_HPP_INCLUDED
#define DOT_HPP_INCLUDED

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// Qt
#include <QTextStream>
#include <QVector>

namespace Kilo
{
    class Dot;
}

// libkilo
#include <libkilo/AbstractParticle.hpp>
#include <libkilo/ParticleField.hpp>
#include <libkilo/Color.hpp>

namespace Kilo
{
    class Dot : public AbstractParticle
    {
        Q_OBJECT

        private:
            Color       _color;
            long double _charge;
            long double _mass;
            long double _radius;

        public:
            Q_INVOKABLE Dot();
            ~Dot();

            Color       getColor () const override;
            long double getCharge() const override;
            long double getMass  () const override;
            long double getRadius() const override;
    };
}

#endif // DOT_HPP_INCLUDED
