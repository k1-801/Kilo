﻿#ifndef COLOR_HPP
#define COLOR_HPP

// C
#include <stdint.h>

namespace Kilo
{
    class Color
    {
        private:
            uint8_t r, g, b, a;

        public:
            Color();
            Color(uint8_t, uint8_t, uint8_t, uint8_t = 255);
            // Getters
            uint8_t   red() const;
            uint8_t green() const;
            uint8_t  blue() const;
            uint8_t alpha() const;

            float   redF() const;
            float greenF() const;
            float  blueF() const;
            float alphaF() const;

            // Setters
            void setRed  (uint8_t);
            void setGreen(uint8_t);
            void setBlue (uint8_t);
            void setAlpha(uint8_t);

            void setRedF  (float);
            void setGreenF(float);
            void setBlueF (float);
            void setAlphaF(float);

            void setRgb(uint8_t, uint8_t, uint8_t, uint8_t = 255);
    };
}

#endif // COLOR_HPP
