﻿#pragma once

#ifndef KILO_PARTICLE_HPP_INCLUDED
#define KILO_PARTICLE_HPP_INCLUDED

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// C
#include <stdint.h>
// C++
#include <list>
#include <memory>
#include <vector>
// Qt
#include <QTextStream>
#include <QObject>
#include <QString>
#include <QVector>
// libkilo
#include <libkilo/Vector3.hpp>
#include <libkilo/Color.hpp>

namespace Kilo
{
	class AbstractParticle;
	typedef std::shared_ptr<AbstractParticle> ParticleSP;
	typedef std::  weak_ptr<AbstractParticle> ParticleWP;
}

// libilo, NEEDS declarations above
#include <libkilo/ParticleField.hpp>

namespace Kilo
{
	class AbstractParticle : public QObject, public std::enable_shared_from_this<AbstractParticle>
	{
		Q_OBJECT

		private:
			int _sc_index;

		protected:
			QList<ParticleSP>       _children;
			QVector<ParticleField*> _fields;
			ParticleWP              _parent;
			// Current speed and position, relative to the parent's ones
			Speed                   _speed;
			Coord                   _coord;
			Force                   _force;

			void _addChild(ParticleWP);
			void _delChild(ParticleWP);

		public:
			AbstractParticle();
			virtual ~AbstractParticle();

			void clearChildren();

			virtual Color            getColor    () const = 0;
			virtual long double      getCharge   () const;
			QList<ParticleSP>&       getChildren ();
			Coord                    getCoord    () const;
			QVector<ParticleField*>& getFields   ();
			virtual long double      getMass     () const = 0;
			QString                  getName     () const;
			ParticleWP               getParent   () const;
			virtual long double      getRadius   () const = 0;
			virtual Speed            getSpeed    () const;

			void  readData(QTextStream&);
			void writeData(QTextStream&) const;

			void setParent(ParticleWP);

			// These functions MAY be defined by user to make additional calculations
			// They will be called AFTER basic calculations such as gravity, qulon, etc.
			virtual void updateForce(); // If your particle can have additional forces (fuel speedup, quantum forces), add them here
			virtual void updateCoord();
			virtual void updateGroup(); // Used in Universe for moving it to (0;0;0); May be used somewhere else
			// These functions do the BASIC calculations for the functions above; Each is recursive
			void updateForceBasic(); // Add Gravity and Qulon forces
			void updateCoordBasic(); // Calculate speed and move the particle as for the current speed
			void updateGroupBasic();

			ParticleSP self(); // Fuck yourself
	};
}

#endif // KILO_PARTICLE_HPP_INCLUDED
