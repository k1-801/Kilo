﻿#include "ParticleGroup.hpp"

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// HCL
#include "Factory.hpp"

REGISTER_IN_FACTORY(Kilo, AbstractParticle, Kilo, ParticleGroup)

namespace Kilo
{
	ParticleGroup::ParticleGroup() : AbstractParticle()
	{
		_fields.push_back(new ParticleFieldLongDouble(tr("Mass"),   false, _mass));
		_fields.push_back(new ParticleFieldLongDouble(tr("Radius"), false, _radius));
	}
	ParticleGroup::~ParticleGroup(){}

	long double ParticleGroup::getCharge() const {return _charge;}
	Color       ParticleGroup::getColor () const {return _color;}
	long double ParticleGroup::getMass  () const {return _mass;}
	long double ParticleGroup::getRadius() const {return _radius;}

	void ParticleGroup::updateCoord()
	{
		for(std::shared_ptr<AbstractParticle> i : _children)
		{
			i->updateCoord();
		}
	}

	void ParticleGroup::updateForce()
	{
		for(ParticleSP i : _children)
		{
			i->updateForce();
		}
		_force = 0;
	}

	void ParticleGroup::updateGroup()
	{
		ParticleSP p = _parent.lock();
		if(!p)
		{
			throw Exception("Particle with no parent given - how could this happen?");
		}

		// Update children
		for(ParticleSP c : _children)
		{
			c->updateGroup();
		}

		// Update current group

		// Check siblings for possible merging
		for(ParticleSP c : p->getChildren())
		{
			if((c->getCoord() - getCoord()).length() < _radius)
			{
				// MERGE!!!1!
			}
		}

		// State variales setting
		_charge = 0;
		_color  = Color(0, 0, 0);
		_coord  = 0;
		_mass   = 0;
		_radius = 0;

		uint64_t sr = 0, sg = 0, sb = 0;
		for(ParticleSP c : _children)
		{
			_charge += c->getCharge();
			_coord  += c->getCoord() * c->getMass();
			_mass   += c->getMass ();
			sr += c->getColor().red();
			sg += c->getColor().green();
			sb += c->getColor().blue();
		}
		_color.setRgb(sr / _children.size(), sg / _children.size(), sb / _children.size());
		_coord /= _children.size() * _mass;

		for(ParticleSP c : _children)
		{
			long double r = (c->getCoord() - _coord).length() + c->getRadius();
			if( _radius < r)
				_radius = r;
		}
	}
}
