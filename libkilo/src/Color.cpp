﻿#include "Color.hpp"

namespace Kilo
{
    Color::Color()
    {
        r = 0;
        g = 0;
        b = 0;
        a = 0;
    }

    Color::Color(uint8_t sr, uint8_t sg, uint8_t sb, uint8_t sa)
    {
        setRgb(sr, sg, sb, sa);
    }

    uint8_t Color::alpha() const {return a;}
    uint8_t Color::  red() const {return r;}
    uint8_t Color::green() const {return g;}
    uint8_t Color:: blue() const {return b;}

    float Color::alphaF() const {return a / 255;}
    float Color::  redF() const {return r / 255;}
    float Color::greenF() const {return g / 255;}
    float Color:: blueF() const {return b / 255;}

    void Color::setAlpha(uint8_t sa) {a = sa;}
    void Color::setRed  (uint8_t sr) {r = sr;}
    void Color::setGreen(uint8_t sg) {g = sg;}
    void Color::setBlue (uint8_t sb) {b = sb;}

    void Color::setAlphaF(float sa) {a = sa * 255;}
    void Color::setRedF  (float sr) {r = sr * 255;}
    void Color::setGreenF(float sg) {g = sg * 255;}
    void Color::setBlueF (float sb) {b = sb * 255;}

    void Color::setRgb(uint8_t sr, uint8_t sg, uint8_t sb, uint8_t sa)
    {
        r = sr;
        g = sg;
        b = sb;
        a = sa;
    }
}
