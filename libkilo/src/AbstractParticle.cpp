﻿#include "AbstractParticle.hpp"

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// C
#include <math.h>
// C++
#include <memory>
// GL
#include <GL/glu.h>
// HCL
#include "Factory.hpp"
#include "Vector3.hpp"
#include "Operators.hpp"
// Kilo
#include "Phy.hpp"
#include "Defs.hpp"

namespace Kilo
{
	AbstractParticle::AbstractParticle()
	{
		_sc_index = 0;
		_parent.reset();
		_fields.push_back(new ParticleFieldVector3 (tr("Coord"),  true, _coord));
		_fields.push_back(new ParticleFieldVector3 (tr("Force"),  true, _force));
		_fields.push_back(new ParticleFieldVector3 (tr("Speed"),  true, _speed));
	}
	AbstractParticle::~AbstractParticle(){}

	void AbstractParticle::_addChild(ParticleWP p)
	{
		ParticleSP pw = p.lock();
		if(pw)
		{
			ParticleSP pp = pw->getParent().lock();
			if(pp != self())
			{
				pw->_parent = self();
				_children.push_back(pw);
				if(pp)
				{
					pp->_delChild(p);
				}
			}
		}
		else
		{
			throw Exception("Null pointer");
		}
	}

	void AbstractParticle::_delChild(ParticleWP p)
	{
		ParticleSP ps = p.lock();
		if(ps)
		{
			QList<ParticleSP>::iterator it;
			for(it = _children.begin(); it != _children.end(); ++it)
			{
				if(ps == *it)
				{
					_children.erase(it);
					break;
				}
			}
		}
		else
		{
			throw Exception("Null pointer");
		}
	}

	void AbstractParticle::clearChildren()
	{
		for(ParticleSP i : _children)
		{
			i.reset();
		}
		_children.clear();
	}

	QVector<ParticleField*>& AbstractParticle::getFields()
	{
		return _fields;
	}

	long double        AbstractParticle::getCharge   () const {return 0;}
	QList<ParticleSP>& AbstractParticle::getChildren ()       {return _children;}
	Coord              AbstractParticle::getCoord    () const {return _coord;}
	QString            AbstractParticle::getName     () const {return metaObject()->className();}
	ParticleWP         AbstractParticle::getParent   () const {return _parent;}
	Speed              AbstractParticle::getSpeed    () const {return _speed;}

	void AbstractParticle::writeData(QTextStream& str) const
	{
		for(ParticleField* i: _fields)
		{
			QString* tmp = i->getValue();
			str << *tmp;
			delete tmp;
		}
		str << _children.size() << '\n';
		for(ParticleSP i: _children)
		{
			str << i->getName() << ' ';
			i->writeData(str);
			str << '\n';
		}
	}

	void AbstractParticle::readData(QTextStream& str)
	{
		int i, n;
		QString tmpread;
		clearChildren();
		str >> n;

		for(ParticleField* i: _fields)
		{
			str >> tmpread;
			i->setValue(tmpread);
		}
		for(i = 0; i < n; ++i)
		{
			str >> tmpread;
			ParticleSP c = Factory<AbstractParticle>::getInstance().get(tmpread);
			c->setParent(self());
			c->readData(str);
		}
		updateGroup();
	}

	void AbstractParticle::setParent(ParticleWP p)
	{
		ParticleSP parent_new = p.lock();
		ParticleSP parent_old = _parent.lock();

		if(parent_new != parent_old)
		{
			if(parent_new)
			{
				parent_new->_addChild(self());
			}
			if(parent_old)
			{
				parent_old->_delChild(self());
			}
		}
		_parent = parent_new;
	}

	ParticleSP AbstractParticle::self()
	{
		return shared_from_this();
	}

	// Basic calculation functions
	void AbstractParticle::updateForceBasic()
	{
		// First, run for subparticles
		for(ParticleSP child: _children)
		{
			child->updateForceBasic();
		}
		// Now, do the common job

		ParticleSP p;
		ParticleSP prev = shared_from_this();
		long double m0 = getMass(), m1;
		long double q0 = getCharge(), q1;
		_force = 0;
		for(p = _parent.lock(); p; p = p->getParent().lock())
		{
			for(ParticleSP c : p->getChildren())
			{
				if(c != prev)
				{
					Vector3 diff = getCoord() - c->getCoord();
					long double dl = diff.length();
					q1 = c->getCharge();
					m1 = c->getMass();
					Force qforce =  (diff * PHY_K * q0 * q1) / (dl * dl * dl); // Qulon
					Force mforce = -(diff * PHY_G * m0 * m1) / (dl * dl * dl); // Gravity
					_force += qforce + mforce;
				}
			}
			prev = p;
		};

		// Now, run the particle-dependent job
		this->updateForce();
	}

	void AbstractParticle::updateCoordBasic()
	{
		long double step = 1;
		_speed += _force / step / step;
	}

	// Particle grouping is not realized, but should be ready to be done
	void AbstractParticle::updateGroupBasic(){}

	// EMPTY functions just for being called after basic
	void AbstractParticle::updateCoord(){}
	void AbstractParticle::updateForce(){}
	void AbstractParticle::updateGroup(){}
}
