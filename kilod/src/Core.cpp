﻿#include "Core.hpp"

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// C++
#include <math.h>
// Qt
#include <QCoreApplication>
#include <QFile>
// HCL
#include <libkilo/Exception.hpp>
// libkilo
#include <libkilo/Phy.hpp>
#include <libkilo/Dot.hpp>
// Kilo
#include "Runner.hpp"

namespace Kilo
{
	Core::Core()
	{
		_precision = 1;
	}

	Core::~Core()
	{
		//Runner::getInstance().stop();
		//mutexParticles.unlock();
	}

	Core& Core::getInstance()
	{
		static Core _instance;
		return _instance;
	}

	// Mess next

	void Core::run()
	{
		// This is the Runner thread initialization
		// However, we are rewriting it to run threads in a pool, so...
		/*
		Runner  * r = &Runner  ::getInstance();
		r->moveToThread(&_runner);
		connect(&_runner, &QThread::started,  r,    &Runner ::start);
		connect(&_runner, &QThread::finished, r,    &QObject::deleteLater);
		connect(r,        &Runner ::finished, this, &Core   ::runnerFinished);
		connect(this,     &Core   ::start,    r,    &Runner ::simRun);
		connect(this,     &Core   ::stop,     r,    &Runner ::stop);
		_runner.start();*/

		// This is the command line parsing
		/*
		int i;
		bool opened = false, autostart = false;
		QStringList args = QCoreApplication::arguments();
		for(i = 1; i < args.size(); ++i)
		{
			QString arg = args[i];
			if(!opened && !arg.startsWith("--"))
			{
				open(arg);
				opened = true;
				continue;
			}
			if(arg == "--autostart")
			{
				autostart = true;
				continue;
			}
			if(arg == "--noautostart")
			{
				autostart = false;
				continue;
			}
			if(arg == "--precision")
			{
				++i;
				if(i >= args.size() || !(_precision = arg.toDouble()))
					error(tr("Failed to set precision"));
			}
		}
		if(autostart)
		{
			emit start();
		}
			*/
	}

	float Core::getPrecision() const
	{
		return _precision;
	}

	QString Core::getPath()
	{
		return _path;
	}

	void Core::clear()
	{
		// We need to stop the simulation before cleaning, and WAIT the simulation to be stopped.
		emit stop();
	}

	void Core::error(QString)
	{
		// This function should notify the client about the error, write it to the log, print it on the console, etc.
		//if(text == QString::null){text = tr("Unknown error");}
	}

/*    void Core::open(QString p)
	{
		QFile f(p);
		if(!f.open(QFile::ReadOnly))
		{
			error(tr("Failed to open file"));
			return;
		}
		if(f.read(4) != "kmf.")
		{
			error(tr("File is not a Kilo Model File"));
			return;
		}
		_path = p;
		QTextStream str(&f);

		// These values are UNUSED, but should be read or skipped.
		// Save-file format should be changed to INI.
		bool spheres_b, traectories_b, smartclean_b, autoclean_b, traectories;
		long double zoom, smartclean;
		Coord cam;

		str >> traectories >> _precision >> zoom >> smartclean >> autoclean_b >> spheres_b >> traectories_b >> smartclean_b >> cam;


		Universe::getInstance().readData(str);
		f.close();
	}

	void Core::save(QString p)
	{
		// TODO: check extension
		try
		{
			QFile f(p + '~');
			if(!f.open(QFile::WriteOnly))
			{
				error(tr("Failed to open file"));
				return;
			}
			QTextStream str(&f);
			str.setRealNumberNotation(QTextStream::FixedNotation);
			str.setRealNumberPrecision(16);
			str << "kmf.0 " << _precision << " 0 0 0 0 0 0 " << _cam <<"\n";
			Universe::getInstance().writeData(str);
			str.flush();
			f.close();
			if(QFile::exists(p) && !QFile::remove(p))
			{
				error(tr("Failed to remove old file"));
				return;
			}
			f.rename(p);
			_path = p;
		}
		CATCH("Core::save");
	}*/

	void Core::runnerFinished()
	{
		_runner.exit();
		_runner.wait();
	}

	void Core::quit()
	{
		emit stop();
		_runner.wait();
		//_runner.exit();
		QCoreApplication::exit();
	}
}
