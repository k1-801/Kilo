﻿#include <QCoreApplication>

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 * Server/daemon application
 *
 * @project Kilo
 * @author k1-801
 */

#include "Core.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Kilo::Core::getInstance().run();
    return a.exec();
}
