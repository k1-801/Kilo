QT += core
QT -= gui

CONFIG += c++11

TARGET = kilod
CONFIG += console
CONFIG -= app_bundle
INCLUDEPATH += /usr/include/c++/6.2.1/ /usr/include/ ./include/ ../include/

TEMPLATE = app

SOURCES += \
	src/main.cpp \
	src/Core.cpp \
	src/Runner.cpp

HEADERS += \
	include/Runner.hpp \
	include/Core.hpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libkilo/release/ -lkilo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libkilo/debug/ -lkilo
else:unix: LIBS += -L$$OUT_PWD/../libkilo/ -lkilo
