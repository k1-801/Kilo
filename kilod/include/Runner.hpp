﻿#ifndef RUNNERTHREAD_HPP
#define RUNNERTHREAD_HPP

// C
#include <stdint.h>
// Qt
#include <QObject>

namespace Kilo
{
	class Runner : public QObject
	{
		private:
			uint64_t _tid; // Thread ID
			bool _running; // LOL

		public:
			;

		private slots:
			void stepStart(uint8_t, uint64_t);

		signals:
			void stepFinished(uint8_t, uint64_t);
	};
}

#endif // RUNNERTHREAD_HPP
