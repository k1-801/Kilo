﻿#pragma once

#ifndef KILO_CORE_HPP_INCLUDED
#define KILO_CORE_HPP_INCLUDED

/**
 * This file is a part of Kilo project.
 * Kilo - basic physical particles' interaction simulator
 *
 * @project Kilo
 * @author k1-801
 */

// C
#include <stdint.h>
// C++
#include <list>
// Qt
#include <QObject>
#include <QMutex>
#include <QThread>
#include <QThreadPool>
#include <QTranslator>
// Libkilo
#include <libkilo/Universe.hpp>
#include <libkilo/Vector3.hpp>

namespace Kilo
{
	class Core : public QObject
	{
		Q_OBJECT
		private:
			// This one controls the simulation precision and speed
			float     _precision;
			uint64_t  _tick;
			float     _simTime; // This one stores how much simulation time passed from the beginning of the simulation (not real time)
			// add _precision value on each tick
			typedef enum{STOPPED, NONSTOP, STOP_BY_TICK, STOP_BY_TIME} StopEvent;
			StopEvent _stopEvent; // This one controls time-limited simulation mode
			uint64_t  _ticksLeft; // This one controls how many ticks sholud the simulation long
			uint64_t  _threads; // Number of threads to simulate; More threads may calculate big simulation faster on multi-core machines,
			// but interthread communication will be much slower. Also, all of the threads are paused when the client requests
			// to see the universe

			QString     _path; // Is this needed?
			QThread     _runner;
			QThreadPool _pool;
			Coord       _cam;

		protected:
			Core();
			~Core();

		public:
			QMutex mutexParams;
			std::mutex mutexParticles;

			static Core& getInstance();
			QString getPath();
			float getPrecision() const;

			void clear();
			void error(QString);
			void run();

		public slots:
			//void open(QString);
			//void save(QString);
			void quit();

		private slots:
			void runnerFinished();

		signals:
			void stop();
			void start();
	};
}

#define CATCH(fun) \
	catch(Exception& e) \
	{ \
		Core::getInstance().error(tr("%1(): %2").arg(fun).arg(e.getText())); \
	} \
	catch(std::exception& e) \
	{ \
		Core::getInstance().error(tr("%1() cought an std::exception: %2").arg(fun).arg(e.what())); \
	} \
	catch(...) \
	{ \
		Core::getInstance().error(tr("%1() cought an unknown exception").arg(fun)); \
	}

#endif // KILO_CORE_HPP_INCLUDED
